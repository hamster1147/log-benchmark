/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.robot;

import java.io.IOException;
import java.util.function.Consumer;

import org.growingstems.frc.util.WpiTimeSource;
import org.growingstems.measurements.Measurements.Time;
import org.growingstems.util.logger.DataHiveLogger;
import org.growingstems.util.timer.Timer;

import edu.wpi.first.wpilibj.DriverStation;

public class Robot extends RobotRunnerBase {
    private final LogBuilder m_logBuilder = new LogBuilder("Test");
    private DataHiveLogger m_logger = null;
    private final Consumer<Time> m_timeEntry;
    private final Timer m_timer = new WpiTimeSource().createTimer();

    public Robot() {
        m_timeEntry = m_logBuilder.makeSyncLogEntry("Timing", m_logBuilder.timeType_us, Time.ZERO);
        try {
            m_logger = m_logBuilder.init();
        } catch (IOException e) {
            DriverStation.reportError("Logger failed to initialize!", true);
        }
        m_timer.start();
    }

    @Override
    protected void robotInit() {
    }

    @Override
    protected void emergencyStopped() {
    }

    @Override
    protected void robotPeriodic() {
        var time = m_timer.reset();
        m_timeEntry.accept(time);
        m_logger.update();
    }

    @Override
    protected void disabledInit() {
    }

    @Override
    protected void disabledPeriodic() {
    }

    @Override
    protected void disabledExit() {
    }

    @Override
    protected void autonomousInit() {
    }

    @Override
    protected void autonomousPeriodic() {
    }

    @Override
    protected void autonomousExit() {
    }

    @Override
    protected void teleopInit() {
    }

    @Override
    protected void teleopPeriodic() {
    }

    @Override
    protected void teleopExit() {
    }

    @Override
    protected void testInit() {
    }

    @Override
    protected void testPeriodic() {
    }

    @Override
    protected void testExit() {
    }

}
