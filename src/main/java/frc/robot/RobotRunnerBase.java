/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.robot;

import org.growingstems.frc.util.RobotMatchState;
import org.growingstems.frc.util.RobotMatchState.MatchMode;
import org.growingstems.frc.util.RobotMatchState.MatchState;

import edu.wpi.first.hal.DriverStationJNI;
import edu.wpi.first.util.WPIUtilJNI;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.RobotBase;
import edu.wpi.first.wpilibj.internal.DriverStationModeThread;

/**
 * The VM is configured to automatically run this class. If you change the name of this class or the
 * package after creating this project, you must also update the build.gradle file in the project.
 */
public abstract class RobotRunnerBase extends RobotBase {
  protected abstract void robotInit();

  protected abstract void emergencyStopped();

  protected abstract void robotPeriodic();

  protected abstract void disabledInit();

  protected abstract void disabledPeriodic();

  protected abstract void disabledExit();

  protected abstract void autonomousInit();

  protected abstract void autonomousPeriodic();

  protected abstract void autonomousExit();

  protected abstract void teleopInit();

  protected abstract void teleopPeriodic();

  protected abstract void teleopExit();

  protected abstract void testInit();

  protected abstract void testPeriodic();

  protected abstract void testExit();

  protected volatile MatchState m_previousMatchState = MatchState.UNKNOWN_DISABLED;
  protected volatile boolean m_exit = false;

  @Override
  public void startCompetition() {
    robotInit();

    DriverStationModeThread modeThread = new DriverStationModeThread();

    int event = WPIUtilJNI.createEvent(false, false);

    DriverStation.provideRefreshedDataEventHandle(event);

    // Tell the DS that the robot is ready to be enabled
    System.out.println("********** Robot program startup complete **********");
    DriverStationJNI.observeUserProgramStarting();

    while (!Thread.currentThread().isInterrupted() && !m_exit) {
        var currentMatchState = RobotMatchState.getMatchState();
        var matchStateChanged = m_previousMatchState != currentMatchState;
        m_previousMatchState = currentMatchState;

        if (matchStateChanged) {
            // Run Exit Functions
            if (m_previousMatchState.enabled) {
                if (m_previousMatchState.matchMode == MatchMode.AUTO) {
                    autonomousExit();
                    modeThread.inAutonomous(false);
                } else if (m_previousMatchState.matchMode == MatchMode.TELE) {
                    teleopExit();
                    modeThread.inTeleop(false);
                } else if (m_previousMatchState.matchMode == MatchMode.TEST) {
                    testExit();
                    modeThread.inTest(false);
                }
            } else {
                disabledExit();
                modeThread.inDisabled(false);
            }

            // Run Init Functions
            if (currentMatchState.enabled) {
                if (currentMatchState.matchMode == MatchMode.AUTO) {
                    modeThread.inAutonomous(true);
                    autonomousInit();
                } else if (currentMatchState.matchMode == MatchMode.TELE) {
                    modeThread.inTeleop(true);
                    teleopInit();
                } else if (currentMatchState.matchMode == MatchMode.TEST) {
                    modeThread.inTest(true);
                    testInit();
                } else if (currentMatchState.matchMode == MatchMode.EMERGENCY_STOPPED) {
                    emergencyStopped();
                }
            } else {
                modeThread.inDisabled(true);
                disabledInit();
            }
        }

        robotPeriodic();

        // Run Periodic Functions
        if (currentMatchState.enabled) {
            if (currentMatchState.matchMode == MatchMode.AUTO) {
                autonomousPeriodic();
            } else if (currentMatchState.matchMode == MatchMode.TELE) {
                teleopPeriodic();
            } else if (currentMatchState.matchMode == MatchMode.TEST) {
                testPeriodic();
            }
        } else {
            disabledPeriodic();
        }

        try {
            Thread.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // try {
        //     WPIUtilJNI.waitForObject(event);
        // } catch (InterruptedException e) {
        //     Thread.interrupt();
        // }
    }

    DriverStation.removeRefreshedDataEventHandle(event);
    modeThread.close();
  }

  @Override
  public void endCompetition() {
    m_exit = true;
  }
}
