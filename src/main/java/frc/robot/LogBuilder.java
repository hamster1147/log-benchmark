/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.robot;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.nio.file.Paths;
import java.util.function.Consumer;

import org.growingstems.frc.util.WpiTimeSource;
import org.growingstems.math.Pose2dU;
import org.growingstems.math.RateCalculator;
import org.growingstems.math.Vector2dU;
import org.growingstems.measurements.Angle;
import org.growingstems.measurements.Measurements.Length;
import org.growingstems.measurements.Measurements.Time;
import org.growingstems.measurements.Measurements.Velocity;
import org.growingstems.util.logger.AsyncLogEntry;
import org.growingstems.util.logger.DataHiveLogger;
import org.growingstems.util.logger.LogEntryType;

import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.Filesystem;

public class LogBuilder extends DataHiveLogger.Builder {
    private static final int k_maxFileAttempts = 10;
    private static final int k_pipeSize = 65536;
    private static final Time k_writeSleepTime = Time.milliseconds(250.0);
    private static final int k_dumpSize = 0;

    private long m_totalTransferred = 0;
    private RateCalculator m_fileWriteRate = new RateCalculator(new WpiTimeSource());

    // Helpful initializers
    public final Vector2dU<Velocity> zeroVelocityVector = new Vector2dU<>(Velocity.ZERO, Velocity.ZERO);
    public final Pose2dU<Length> zeroLengthPose = new Pose2dU<>(Length.ZERO, Length.ZERO, Angle.ZERO);

    public final LogEntryType<Time> timeType_s;
    public final LogEntryType<Time> timeType_ms;
    public final LogEntryType<Time> timeType_us;

    // If defaultValue is `null`, the first value will be logged no matter what.
    // If defaultValue is not null, nothing will be logged until a different value
    // is received
    // Note: This is only valid for Async Log Entries. We may have to change the
    // input type to Consumer<T> to allow for more behavior options in the future,
    // but for now, for safety, this is explicitly requiring an AsyncLogEntry<T>
    public static <T> Consumer<T> makeNoRepeats(AsyncLogEntry<T> asyncConsumer, T defaultValue) {
        return new Consumer<T>() {
            private T prev = defaultValue;

            @Override
            public void accept(T value) {
                // Only log when error code changes
                if (prev != null && value.equals(prev)) {
                    return;
                }

                asyncConsumer.accept(value);
                prev = value;
            }
        };
    }

    public <T> Consumer<T> makeNoRepeatsAsyncEntry(String name, LogEntryType<T> entryType, T defaultValue) {
        return makeNoRepeats(makeAsyncLogEntry(name, entryType), defaultValue);
    }

    protected final File k_logDir = Paths.get(Filesystem.getOperatingDirectory().getPath(), "logs").toFile();
    protected final PipedOutputStream m_outputStream;
    protected File m_logFile;

    public LogBuilder(String initialLogFileName) {
        this(initialLogFileName, new PipedOutputStream());
    }

    private LogBuilder(String initialLogFileName, PipedOutputStream outputStream) {
        super(outputStream, new WpiTimeSource(), "Logger Benchmark");

        m_outputStream = outputStream;
        m_logFile = deconflictFilename(initialLogFileName);

        // Unit Types

        timeType_s = this.<Time>buildGroupType("Time")
                .addMember("seconds", this.doubleType, Time::asSeconds)
                .register();
        timeType_ms = this.<Time>buildGroupType("Time")
                .addMember("milliseconds", this.doubleType, Time::asMilliseconds)
                .register();
        timeType_us = this.<Time>buildGroupType("Time")
                .addMember("microseconds", this.doubleType, Time::asMicroseconds)
                .register();
    }

    @Override
    public DataHiveLogger init() throws IOException {
        // Ensure log directory exists
        k_logDir.mkdir();

        @SuppressWarnings("resource") // We never close it because we continue logging until the robot turns off
        var inputStream = new PipedInputStream(m_outputStream, k_pipeSize);
        @SuppressWarnings("resource") // We never close it because we continue logging until the robot turns off
        var fileStream = new FileOutputStream(m_logFile);
        // Make separate thread transfer data to writing stream
        new Thread() {
            public void run() {
                while (true) {
                    try {
                        var avail = inputStream.available();
                        if (avail > k_dumpSize) {
                            fileStream.write(inputStream.readNBytes(avail));

                            m_totalTransferred += avail;
                            var rate = m_fileWriteRate.update((double)m_totalTransferred);
                        }
                    } catch (IOException e) {
                        DriverStation.reportError("Error occurred while writing", e.getStackTrace());
                    }

                    try {
                        Thread.sleep((long) k_writeSleepTime.asMilliseconds());
                    } catch (InterruptedException e) {
                        DriverStation.reportError("Log thread interrupted while sleeping", e.getStackTrace());
                    }
                }
            }
        }.start();

        return super.init();
    }

    private File stringToFileHandle(String logName) {
        return new File(k_logDir, logName + ".dhl");
    }

    /**
     * Creates a File object for a new file with the given name.. DO NOT INCLUDE THE
     * EXTENSION
     *
     * @param logName The extension-less log file name
     * @return A File object that is not already used (unless the max attempts are
     *         used)
     */
    private File deconflictFilename(String logName) {
        int i = 0;
        File file = stringToFileHandle(logName);
        do {
            if (file.exists()) {
                file = stringToFileHandle(logName + "_" + Integer.toString(++i));
            } else {
                return file;
            }
        } while (i < k_maxFileAttempts);
        return stringToFileHandle(logName + "_MAX");
    }

    /**
     * Renames the log file. DO NOT INCLUDE THE EXTENSION
     *
     * @param logName The extension-less log file name
     * @return True if the rename was successful
     */
    public boolean renameTo(String logName) {
        File newFile = deconflictFilename(logName);
        boolean success = m_logFile.renameTo(newFile);
        if (success) {
            m_logFile = newFile;
        }
        return success;
    }
}
